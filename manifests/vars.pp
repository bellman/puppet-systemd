# Copyright © 2021   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Various constants (file paths, et.c).  Not overridable by users.
 * Mostly intended for internal use.
 */
class systemd::vars
{
    $etcdir = '/etc/systemd/system'
}
