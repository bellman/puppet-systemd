# Copyright © 2024        Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Manage a systemd-tmpfiles(8) config file.
 *
 * You want to read the tmpfiles.d(5) manual page to understand what most
 * of the parameters actually do in the end.
 *
 * systemd-tmpfiles is automatically run as part of managing the tmpfiles.d
 * config file, so e.g. any directory the config file says should be created,
 * will be.
 *
 * Example:
 *
 *	systemd::tmpfiles {
 *	    'smurf':
 *		type => 'd', mode => '0750', owner => 'gargamel',
 *		paths => {
 *		    '/run/alldefaults' => {},
 *		    '/run/file' => { 'type' => 'f', 'group' => 'smurfs' },
 *		};
 *	}
 *
 * This will generate the entries
 *
 *	d  /run/alldefaults     0750  gargamel  -       -
 *	f  /run/file            0750  gargamel  smurfs  -
 *
 * in /etc/tmpfiles.d/smurf.conf.  Since the maxage parameter is not
 * specified either in the "defaults", or in the hashes for either of the
 * paths, it will default to "-".
 *
 * Note that there is basically no verification that the parameter values
 * are syntactically correct, so it is possible to create config files
 * that are not valid.
 */
define systemd::tmpfiles(

	# Path(s) to manage, and their parameters.
	#
	# Simple case, this is a path or a (nested) list of paths to put
	# into the tmpfiles.d config file.  The 'type', 'mode', 'owner',
	# 'group', 'maxage' and 'argument' parameters will apply equally to
	# all paths.
	#
	# Alternatively, this can be a hash of hashes, mapping from paths to
	# a hash of parameter values for each path, e.g.
	#
	#     paths => {
	#         '/run/foo' => { 'type' => 'd', 'mode' => '0700' },
	#         '/run/bar' => { 'type' => 'f', 'owner' => 'zork' },
	#     }
	#
	# In this case, the resource parameters will act as defaults for
	# parameters missing in a specific path's parameter hash.
	#
	# The 'type' parameter must be specified for every path, either in
	# its parameter hash, or as the $type parameter on the resource.
	# All other parameters defaults to "-" or nothing.
	#
	# One limitation is that you can only have one entry per path.  If
	# you e.g. need one "d" entry and one "a" (ACL) entry for the same
	# path, you must either use the $content parameter, forgoing some of
	# the abstraction here, or you have to use more than one resource,
	# putting the entries in different tmpfiles.d config files.
	#
	$paths = {},

	# The entry type for the paths in $paths, unless overridden in the
	# path-specific parameter hash.
	#
	# The entry type must be specified for all paths, either in the
	# path's parameter hash in $paths, or using this parameter.
	#
	$type = undef,

	# The values for the mode, uid, gid, age, and argument fields,
	# respectively, of the tmpfiles.d config entries for $paths, unless
	# overridden in the path-specific parameter hash in.
	#
	# If a parameter is not specified, either here or in the parameter
	# hash, it defaults to "-" (or to "" for the $argument parameter).
	#
	$mode = undef,
	$owner = undef,
	$group = undef,
	$maxage = undef,
	$argument = undef,

	# Alternative to specifying paths to manage using parameters above.
	# One or more lines that will be written as-is to the tmpfiles.d
	# file.
	# While it says "lines", this can actually be any string, if you
	# e.g. a template generating several lines as a single string.
	#
	$content = [],

	# Comments to prepend to the generated tmpfiles.d config file.
	#
	$comment = [],

	# One of 'present' or 'absent'.
	#
	# This only controls if the config file in /etc/tmpfiles.d should
	# be present or removed.  In particular, setting this to 'absent'
	# will only remove the config file itself, but won't remove the
	# files/directories specified by the config file.
	#
	$ensure = 'present',
)
{
    $resource_ref = "Systemd::Tmpfiles[${title}]"

    case $ensure
    {
	'present': {
	    if (($type) and ($paths == [] or $paths == {})) {
		fail("${resource_ref}: type parameter set, but no paths")
	    }
	    if (($paths == [] or $paths == {}) and
		($content == '' or $content == []))
	    {
		fail("${resource_ref}: both paths and content are empty")
	    }
	    contain systemd::tmpfiles::trigger

	    $cfgfile_content = template('systemd/tmpfiles.conf.erb')
	    file {
		"/etc/tmpfiles.d/${name}.conf":
		    ensure => file,
		    content => $cfgfile_content,
		    owner => 'root', group => 'root', mode => '0644',
		    notify => Exec['systemd::tmpfiles::trigger'];
	    }
	}

	'absent': {
	    file {
		"/etc/tmpfiles.d/${name}.conf":
		    ensure => absent;
	    }
	}

	default: {
	    fail("${resource_ref}: Bad ensure parameter, ${ensure}")
	}
    }
}


class systemd::tmpfiles::trigger
{
    exec {
	'systemd::tmpfiles::trigger':
	    command => shellquote(
		'systemd-tmpfiles', '--create', '--clean'),
	    path => [ '/bin', '/usr/bin', '/sbin', '/usr/sbin', ],
	    refreshonly => true;
    }
}
