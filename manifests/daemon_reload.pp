# Copyright © 2021   Thomas Bellman, Linköping, Sweden
# Licensed under the GNU LGPL v3+; see the README file for more information.


/*
 * Tell systemd to re-read its service configuration files.
 *
 * If you change any systemd service configuration files, you should
 * include and notify this class.  Example:
 *
 *     include systemd::daemon_reload
 *     file {
 *         '/etc/systemd/system/foo.service.d/bar.conf':
 *             ensure => absent,
 *             notify => Class['systemd::daemon_reload'];
 *     }
 *
 * The systemd::unit and systemd::unit_options definitions includes
 * and notifies this class automatically, so users do not need to do
 * that themselves.
 */
class systemd::daemon_reload
{
    exec {
	'systemd::daemon_reload::do-reload':
	    command => 'systemctl daemon-reload',
	    path => [ '/bin', '/usr/bin', '/sbin', '/usr/sbin', ],
	    refreshonly => true;
    }
}
